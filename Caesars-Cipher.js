function rot13(str) {

    let word = str;
    let code = [];
    
  
    for (let i = 0; i < word.length; i++) {
      code.push(word[i]);
    }
  
    let rot13code = "";
  
    for (let j = 0; j < code.length; j++) {
      switch (word[j]) {
        case "A":
        rot13code += "N";
        break;
        case "B":
        rot13code += "O";
        break;
        case "C":
        rot13code += "P";
        break;
        case "D":
        rot13code += "Q";
        break;
        case "E":
        rot13code += "R";
        break;
        case "F":
        rot13code += "S";
        break;
        case "G":
        rot13code += "T";
        break;
        case "H":
        rot13code += "U";
        break;
        case "I":
        rot13code += "V";
        break;
        case "J":
        rot13code += "W";
        break;
        case "K":
        rot13code += "X";
        break;
        case "L":
        rot13code += "Y";
        break;
        case "M":
        rot13code += "Z";
        break;
        case "N":
        rot13code += "A";
        break;
        case "O":
        rot13code += "B";
        break;
        case "P":
        rot13code += "C";
        break;
        case "Q":
        rot13code += "D";
        break;
        case "R":
        rot13code += "E";
        break;
        case "S":
        rot13code += "F";
        break;
        case "T":
        rot13code += "G";
        break;
        case "U":
        rot13code += "H";
        break;
        case "V":
        rot13code += "I";
        break;
        case "W":
        rot13code += "J";
        break;
        case "X":
        rot13code += "K";
        break;
        case "Y":
        rot13code += "L";
        break;
        case "Z":
        rot13code += "M";
        break;
        case " ":
        rot13code += " ";
        break;
        case ".":
        rot13code += ".";
        break;
        case "?":
        rot13code += "?";
        break;
        case "!":
        rot13code += "!";
        break;
      }
    }
  
    return rot13code;
  }
  
  
  
  console.log(rot13("ADAM KRUK"));